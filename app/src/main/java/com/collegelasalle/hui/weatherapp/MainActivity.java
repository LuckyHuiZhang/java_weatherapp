package com.collegelasalle.hui.weatherapp;
//Hui Zhang

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private String CityName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText txtCityName = findViewById(R.id.txtCityName);

        // Reading from shared preferences
        SharedPreferences getSharedData = getSharedPreferences("city", Context.MODE_PRIVATE);
        txtCityName.setText(getSharedData.getString("city_name","Enter City Name"));


        Button btnGetWeather = (Button)findViewById(R.id.btnGetWeather);
        btnGetWeather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText txtCityName = findViewById(R.id.txtCityName);
                TextView tvWeatherInfo = (TextView) findViewById(R.id.tvMain);

                //Saving Shared Preferences
                SharedPreferences shPref = getSharedPreferences("city", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = shPref.edit();
                editor.putString("city_name", CityName);
                editor.apply();

                //get to next activity
                Intent weather = new Intent(getApplicationContext(), Weather.class);
                weather.putExtra("cities", CityName);
                startActivity(weather);

            }
        });
    }

}
