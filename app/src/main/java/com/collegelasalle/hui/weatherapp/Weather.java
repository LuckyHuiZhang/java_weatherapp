package com.collegelasalle.hui.weatherapp;
//Hui Zhang
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class Weather extends AppCompatActivity {

    TextView tvMain, tvDescription, tvCountry, tvTemp_min, tvTemp_max, tvName, tvTemp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        tvMain = (TextView)findViewById(R.id.tvMain);
        tvDescription = (TextView)findViewById(R.id.tvDescription);
        tvTemp = (TextView)findViewById(R.id.tvTemp);
        tvTemp_min = (TextView)findViewById(R.id.tvTemp_min);
        tvTemp_max = (TextView)findViewById(R.id.tvTemp_max);
        tvCountry = (TextView)findViewById(R.id.tvCountry);
        tvName = (TextView)findViewById(R.id.tvName);
        EditText txtCityName = findViewById(R.id.txtCityName);

        //function: get weather API
        GetWeather();

    }

    public void GetWeather()
    {
        String url = "https://api.openweathermap.org/data/2.5/weather?q=Montreal,Canada&appid=158c5bb7810be8d66b61d4ff0c1e6887&units=metric";
        String uri = Uri.parse(url).buildUpon().build().toString();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET, uri, new Response.Listener<String> (){
            @Override
            public void onResponse(String result) {

                try {

                    JSONObject jsonObj = new JSONObject(result);
                    JSONObject main = jsonObj.getJSONObject("main");
                    JSONObject sys = jsonObj.getJSONObject("sys");
                    JSONObject wind = jsonObj.getJSONObject("wind");
                    JSONObject weather = jsonObj.getJSONArray("weather").getJSONObject(0);

                    Long updatedAt = jsonObj.getLong("dt");
                    String temp = main.getString("temp") + "°C";
                    String temp_min = "Min Temp: " + main.getString("temp_min") + "°C";
                    String temp_max = "Max Temp: " + main.getString("temp_max") + "°C";
                    String description = weather.getString("description");
                    String mainW = weather.getString("main");
                    String country = sys.getString("country");
                    String name = jsonObj.getString("name");


                    tvMain.setText(mainW);
                    tvDescription.setText(description);
                    tvCountry.setText(country);
                    tvName.setText(name);
                    tvTemp.setText(temp);
                    tvTemp_min.setText(temp_min);
                    tvTemp_max.setText(temp_max);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", error.toString());
            }
        }
        );
        requestQueue.add(stringRequest);

    }

}

